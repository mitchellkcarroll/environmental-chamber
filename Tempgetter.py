from machine import Pin,PWM
from Temp_driver import temp
import utime

percent = 10
duty = int(1023*percent/100)

peltiers = PWM(Pin(18))

peltiers.duty(duty)

LED = PWM(Pin(2))

LED.duty(duty)

T1 = Pin(33)
T2 = Pin(32)
T3 = Pin(35)
T4 = Pin(34)

# while True:
# #     cold_side = temp(T1)
# #     hot_side = temp(T2)
# #     ambient = temp(T3)
#     print('==========')
#     print('Cold Side: ' + str(cold_side))
#     print('Hot Side: ' + str(hot_side))
#     print('Ambient : ' + str(ambient))
#     utime.sleep(1)