# -*- coding: utf-8 -*-
"""
Created on Tue Jun  1 09:27:55 2021

@author: camer
"""
import time
def check_numb(numb):
    '''
    @brief  A function that makes sure the inputted value is a number
    @param  numb    The number to make sure is a number
    '''
    num = numb.encode('ascii')  
    l = len(num)
    dec_count = 0
    is_numb = True
    
    for n in range(l):
        # Check for valid frequency
        if not (num[n] >= ord ('0')) & (num[n] <= ord('9')): 
            #print('Not a number')
            if num[n] == ord('.'):
                dec_count += 1
                if dec_count > 1:
                    is_numb = False
                    #print('Decimal Problem')
                    return False
            elif n == 0:
                if num[n] != ord('-'):
                    #print('Not a number')
                    is_numb = False
                    return False
            else:
                #print('Not a number')
                is_numb = False
                return False
    return is_numb

if __name__ == "__main__":
    print(check_numb('-100'))
    time.sleep(50)