# -*- coding: utf-8 -*-
"""
Created on Sat Apr 24 16:30:12 2021

@author: camer
"""
import serial
import numpy as np
import os
from coopinput import coopinput
import csv

class UI:
    
    INIT = 0
    SEND = 1
    WAIT = 2
    DOWNLOAD = 3
    
    def __init__(self,port,baudrate):
        
        self.ser = serial.Serial(port=port,baudrate=baudrate,bytesize=8, parity='N', stopbits=1,timeout=None)
        #self.ser = serial.Serial(port=port,baudrate=baudrate,timeout=None)
        
        self.t = np.array([])
        self.T1 = np.array([])
        self.T2 = np.array([])
        self.T3 = np.array([])
        
        self.state = self.INIT
        
        
    def run(self):
           
        if self.state == self.INIT:
            #self.state = self.WAIT
            self.state = self.SEND
            
            self.run_count = 1
            self.line = ''
            self.first = True
            print('Input Step Response: ')
        elif self.state == self.WAIT:
            (self.line,end) = coopinput(self.line)
            if end:
                self.ser.read_all()
                end = False

                if self.check_numb(self.line):
                    it = self.line.encode('ascii')
                    print(it)
                    self.ser.write(it) # Help, this is not good maybee
                    if not self.first:
                        self.state = self.SEND
                    else:
                        print('Input Step Response: ')
                        self.first = False
                else:
                    print('Input Step Response: ')
                self.line = ''
        elif self.state == self.SEND:
            
            #print(self.ser.in_waiting)
            if self.ser.in_waiting > 0: 
                
                thing = self.ser.readline()
                thing2 = bytearray(''.encode('ascii'))
                for i in thing:
                    if i < 0x7F:
                        thing2 += bytearray(chr(i).encode('ascii'))
                #print(thing)
                string = thing2.decode('ascii')
                
                
                #print(string)
                #string = '1,2,3,4;5,6,7,8'# Just pretending for the moment. Get rid of this soon.
                
                data = string.split(';')
                
                
            
                self.t = np.zeros(len(data))
                self.T1 = np.zeros(len(data))
                self.T2 = np.zeros(len(data))
                self.T3 = np.zeros(len(data))
            
                for i in range(len(data)-1):
                
                    subdata = data[i].split(',')
                    
                    #print(subdata)
                
                    
                    self.t[i] = float(subdata[0])
                    self.T1[i] = float(subdata[1])
                    self.T2[i] = float(subdata[2])
                    self.T3[i] = float(subdata[3])
                    
                ## Process DATA here
                self.download()
                
                print('Data Collected')
                #print('Input Step Response: ')
                
                #self.state = self.WAIT
                
    def check_numb(self, numb):
        '''
        @brief  A function that makes sure the inputted value is a number
        @param  numb    The number to make sure is a number
        '''
        num = numb.encode('ascii')  
        l = len(num)
        dec_count = 0
        is_numb = True
        
        for n in range(l):
            # Check for valid frequency
            if not (num[n] >= ord ('0')) & (num[n] <= ord('9')): 
                #print('Not a number')
                if num[n] == ord('.'):
                    dec_count += 1
                    if dec_count > 1:
                        is_numb = False
                        print('Decimal Problem')
                        return False
                elif n == 0:
                    if num[n] != ord('-'):
                        print('Not a number')
                        is_numb = False
                        return False
                else:
                    print('Not a number')
                    is_numb = False
                    return False
        return is_numb
        
    def download(self):
    
        # field names 
        fields = ['t', 'T1', 'T2', 'T3'] 
    
        # data rows of csv file 
        rows = [self.t,self.T1,self.T2,self.T3]
    
        # name of csv file 
        filename = 'run'+str(self.run_count)+'.csv'
            
        self.run_count += 1
    
        # writing to csv file 
        with open(filename, 'w') as csvfile: 
            # creating a csv writer object 
    
            csvwriter = csv.writer(csvfile,lineterminator='\n') 
        
            # writing the fields 
            csvwriter.writerow(fields) 
        
            # writing the data rows 
            csvwriter.writerows(np.transpose(rows))
    
        os.startfile(filename)


if __name__ == "__main__":
    
    ui1 = UI('COM5',115200)
    
    while True:
        ui1.run()
                
                
                
