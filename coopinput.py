# -*- coding: utf-8 -*-
"""
Created on Wed Apr 28 10:22:03 2021

@author: camer
"""

import serial
import matplotlib as mpl
import time
import sys
import msvcrt
import numpy as np


def coopinput(line):
    '''
    @brief  A function that allows the user to cooperatively interact with the UI
    @details    This function constructs a string as the user inputs charactors 
                and submits the string when the user presses enter. This allows
                the computor to perform actions while the user provides input
    @param  line    The string currently being inputed into the interface
    '''
    user_input = ''
    end = False
    if msvcrt.kbhit():
        user_input = msvcrt.getch()
        if user_input== b'\r':
            end = True
            newline = line
            print('\r')
        elif user_input == b'\b':
            print('\b',end = '')
            sys.stdout.flush()
            newline = line[:-1]
        else:
            user_input = user_input.decode('utf-8')
            print(user_input,end = '')
            sys.stdout.flush()
            newline = line+user_input
    else:
        newline = line
    return (newline, end)

if __name__ == '__main__':
    end =False
    line = ''
    while True:
        (line,end) = coopinput(line)
        if end == True:
            print(len(line))
            line = ''
    
    