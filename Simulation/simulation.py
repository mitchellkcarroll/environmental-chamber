# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 09:07:02 2021

@author: camer
"""

import numpy as np
import matplotlib.pyplot as plt
#import time
import os


class track:
    
    def __init__(self,xi):
        self.First = True
        self.x = xi
        self.d = 0
        self.i = 0
        
    def update(self,x,dt):
        if self.First:
            self.First = False
            self.x = x
        else:
            self.d = (self.x-x)/dt
            self.i += (self.x + x)/2*dt
            self.x = x
    def clear(self):
        self.First = True
        self.d = 0
        self.i = 0

class model:
    
    def __init__(self,Ah,Ac,B,To,Ti,Pi,Qh,Qc):
        self.Ah = Ah
        self.Ac = Ac
        self.B = B
        self.To = np.transpose(np.matrix([0,0,To]))
        self.T = Ti
        
        self.A = Ah
        
        self.P = Pi
        self.Qh = Qh
        self.Qc = Qc
        
        self.ch = -(Ah[2,0]+Ah[2,1]+Ah[2,2])
        self.cc = -(Ac[2,0]+Ac[2,1]+Ac[2,2])
        
    def update(self,u,s,dt):
        if s == 0:
            T = self.T+(self.Ah*(self.T)+self.ch*self.To+self.B*u)*dt
            F = np.matrix(np.identity(3))+self.Ah*dt
            P = F*self.P*np.transpose(F)+self.Qh
            
        else:
            T = self.T+(self.Ac*(self.T)+self.cc*self.To+self.B*u)*dt
            F = np.matrix(np.identity(3))+self.Ac*dt
            P = F*self.P*np.transpose(F)+self.Qc
        self.T = T
        self.P = P

class control:
    
    HPID = 0
    CPID = 1
    NONE = 3
    
    def __init__(self,Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B):
        self.Khp = Khp
        self.Khi = Khi
        self.Khd = Khd
        
        self.Kh = Ah[0,2]/Ah[2,0]*(Ah[2,0]+Ah[2,1]+Ah[2,2])/B[0,0]
        self.Kc = Ac[1,2]/Ac[2,1]*(Ac[2,0]+Ac[2,1]+Ac[2,2])/B[1,1]
        
        self.Kcp = Kcp
        self.Kci = Kci
        self.Kcd = Kcd
        
        self.Vhmax = Vhmax
        self.Vcmax = Vcmax
        self.Khiaw = Khiaw
        self.Kciaw = Kciaw
        
        self.err = track(0)
        
        self.iaw = 0
        
        self.To = To
        
        self.u = np.transpose(np.matrix([0,0]))
        self.s = 0
        self.state = self.HPID
        
        self.sat = False
        
    def update(self,T,Tdes,dt):
        
        self.err.update(Tdes-T[2],dt)
        
        if self.state == self.HPID:
            self.u[0] = self.PID(self.Khp,self.Khi,self.Khd,self.Khiaw,[0,self.Vhmax],self.Kh*(self.To-Tdes))
            self.u[1] = 0
            self.s = 0
            #if (Tdes < self.To) | ():
            if (Tdes>To-1)&(Tdes<To+1)&(T[2]>To-1)&(T[2]<To+1):
                self.state = self.NONE
            elif (1.1*(Tdes-To)+To < T[2]):
                self.state = self.CPID
                self.err.clear()
                
        elif self.state == self.CPID:
            self.u[0] = 0
            self.u[1] = self.PID(self.Kcp,self.Kci,self.Kcd,self.Kciaw,[self.Vcmax,0],self.Kc*(self.To-Tdes))
            self.s = 1
            #if Tdes > self.To:
            if (Tdes>To-1)&(Tdes<To+1)&(T[2]>To-1)&(T[2]<To+1):
                self.state = self.NONE
            elif 1.1*(Tdes-To)+To > T[2]:
                self.state = self.HPID
                self.err.clear()
                
        elif self.state == self.NONE:
            self.u[0] = 0
            self.u[1] = 0
            if (Tdes<To-1)|(Tdes>To+1)|(T[2]<To-1)|(T[2]>To+1):
                if Tdes < T[2]:
                    self.state = self.CPID
                else:
                    self.state = self.HPID
                self.err.clear()
        
    def PID(self,Kp,Ki,Kd,Kiaw,Vrange,K):
        
        #K = 0  #Uncomment to use standard control
        
        Vp = Kp*self.err.x
        Vi = Ki*(self.err.i+self.iaw)
        Vd = Kd*self.err.d
        
        Vraw = Vp+Vi+Vd+K
        
        if Vraw >= Vrange[1]:
            V = Vrange[1]
        elif Vraw <= Vrange[0]:
            V = Vrange[0]
        else:
            V = Vraw
        
        self.iaw = (V-Vraw)*Kiaw
        
        return V
        
class kalman:
    
    def __init__(self,R,chmodel,comodel):
        self.comodel = comodel
        self.chmodel = chmodel
        self.R = R
        self.T = self.chmodel.T
        self.P = self.chmodel.P
        
    def update(self,Tmeas,Tdes,dt):
        
        self.comodel.update(self.T,Tdes,dt)
        self.chmodel.update(self.comodel.u,self.comodel.s,dt)
        
        Tprior = self.chmodel.T
        Pprior = self.chmodel.P
        
        y = Tmeas-Tprior
        S = Pprior+self.R
        K = Pprior*np.linalg.inv(S)
        
        self.T = Tprior+K*y
        self.P = (np.matrix(np.identity(3))-K)*Pprior

        #self.T = Tmeas  # Uncomment to get rid of kalman filtering
        
        self.comodel.T = self.T
        self.chmodel.T = self.T

        
        
        
        
        
def noise(T,P):
    return np.transpose(np.matrix(np.random.multivariate_normal(np.transpose(T).tolist()[0],P)))

def Tdes(t):
    if t < 10:
        return 27
    elif (t >10) &(t<40):
        return 200
    else:
        return 200
    
    
    
Ah = np.matrix([[-10,0,10],
      [0,-1,1],
      [7,2,-9.3]])

Ahact = np.matrix([[-10,0,10],
      [0,-1,1],
      [7,2,-9.35]])
  
Ac = np.matrix([[-1,0,1],
      [0,-10,10],
      [2,7,-9.3]])

Acact = np.matrix([[-1,0,1],
      [0,-10,10],
      [2,7,-9.35]])
  
B = np.matrix([[1,0],
     [0,-1],
     [0,0]])


To = 27
Ti = np.transpose(np.matrix([27,27,27]))


Khp = 20;
Khi = 0.01;
Khd = 0.5;

Khiaw = 0.05;
Vhmax = 120;

Kcp = 20;
Kci = 0.01;
Kcd = 0.5;
Kciaw = 5;
Vcmax = 12;

Pi = np.matrix([[1,0,0],[0,1,0],[0,0,1]])
Qh = np.matrix([[0,0,0],[0,0,0],[0,0,0]])
Qc = np.matrix([[0,0,0],[0,0,0],[0,0,0]])

R = np.matrix([[1,0,0],[0,1,0],[0,0,1]])

chamber = model(Ahact,Acact,B,To,Ti,Pi,Qh,Qc)
cont = control(Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B)
kal = kalman(R,model(Ah,Ac,B,To,Ti,Pi,Qh,Qc),control(Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B))


tmax = 100
dt = 0.05

t = 0

tarray = []
Tarray = []
Tactarray = []
Tdesarray = []
uarray = []
sarray = []

Parray = []

#Z = np.matrix([[1,0,0],[0,1,0],[0,0,1]])
Z = np.matrix([[1,0,0],[0,1,0],[0,0,1]])

print(noise(chamber.T,Z))

T = Ti

while t <= tmax:
    
    Tobs = noise(chamber.T,Z)
    
    
    kal.update(Tobs,Tdes(t),dt)
    cont.update(T,Tdes(t),dt)
    T = kal.T
    
    
    chamber.update(cont.u,cont.s,dt)
    
    tarray += [t]
    Tarray += np.transpose(kal.T).tolist()
    Tactarray += np.transpose(Tobs).tolist()
    
    Tdesarray += [Tdes(t)]
    uarray += np.transpose(cont.u).tolist()
    
    sarray += [cont.s]
    t+=dt
    
    Parray += [kal.P]

foldername = 'plots'

Tch = []
Tact = []
Vh = []

Pa = []

for i in Tarray:
    Tch += [i[2]] 
for i in Tactarray:
    Tact += [i[2]] 
for i in uarray:
    Vh += [i[0]]

for i in Parray:
    Pa += [i[2,2]]
    

plt.figure(1)

plt.plot(tarray,Tdesarray,'b-',linewidth=1,label='Tdesired')
plt.plot(tarray,Tact,'g-',linewidth=1,label='Tchamber')
plt.plot(tarray,Tch,'r-',linewidth=1,label='Tmeasured')

#plt.plot(tarray,Vh,'g-',linewidth=1,label='u')
plt.xlim([0,tmax])

plt.xlabel('Time, [s]')
plt.ylabel('Temp [C]')
plt.legend(loc='best')
plt.savefig('chamber.png')

os.startfile('chamber.png')

plt.figure(2)
plt.plot(tarray,sarray,'r-',linewidth=1,label='switch')
#plt.plot(tarray,Vh,'g-',linewidth=1,label='u')
plt.xlim([0,tmax])

plt.xlabel('Time, [s]')
plt.ylabel('Temp [C]')
plt.legend(loc='best')
plt.savefig('switch.png')

os.startfile('switch.png')


plt.figure(3)
plt.plot(tarray,Pa,'r-',linewidth=1,label='cov')
#plt.plot(tarray,Vh,'g-',linewidth=1,label='u')
plt.xlim([0,tmax])

plt.xlabel('Time, [s]')
plt.ylabel('Cov [C]')
plt.legend(loc='best')
plt.savefig('cov.png')

os.startfile('cov.png')
        