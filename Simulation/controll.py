# -*- coding: utf-8 -*-
"""
Created on Wed Mar 31 09:07:02 2021

@author: camer
"""

import umatrix as np
import ulinalg as npa
import array
import time

import numpy
import matplotlib.pyplot as plt
import os


class track:
    
    def __init__(self,xi):
        self.First = True
        self.x = xi
        self.d = 0
        self.i = 0
        
    def update(self,x,dt):
        if self.First:
            self.First = False
            self.x = x
        else:
            self.d = (self.x-x)/dt
            self.i += (self.x + x)/2*dt
            self.x = x
    def clear(self):
        self.First = True
        self.d = 0
        self.i = 0

class model:
    
    def __init__(self,Ah,Ac,B,To,Ti,Pi,Qh,Qc):
        self.Ah = Ah
        self.Ac = Ac
        self.B = B
        self.To = np.matrix([[0,0,To]]).transpose()
        self.T = Ti
        
        self.A = Ah
        
        self.P = Pi
        self.Qh = Qh
        self.Qc = Qc
        
        self.ch = -(Ah[2,0]+Ah[2,1]+Ah[2,2])
        self.cc = -(Ac[2,0]+Ac[2,1]+Ac[2,2])
        
    def update(self,u,s,dt):
        if s == 0:
            #print(self.T)
            #print(self.Ah)
            #print(self.T)
            #print(self.To)
            #print(self.ch)
            #print(self.B)
            #print(u)
            
            #print(self.Ah)
            #print(self.T)
            #print(npa.dot(self.Ah,self.T))
            
            T = self.T+(npa.dot(self.Ah,self.T)+self.To*self.ch+npa.dot(self.B,u))*dt
            F = npa.eye(3)+self.Ah*dt
            P = npa.dot(npa.dot(F,self.P),F.transpose())+self.Qh
            
        else:
            T = self.T+(npa.dot(self.Ac,self.T)+self.To*self.cc+npa.dot(self.B,u))*dt
            F = npa.eye(3)+self.Ac*dt
            P = npa.dot(npa.dot(F,self.P),F.transpose())+self.Qc
        self.T = T
        self.P = P

class control:
    
    HPID = 0
    CPID = 1
    NONE = 3
    
    def __init__(self,Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B):
        self.Khp = Khp
        self.Khi = Khi
        self.Khd = Khd
        
        self.Kh = Ah[0,2]/Ah[2,0]*(Ah[2,0]+Ah[2,1]+Ah[2,2])/B[0,0]
        self.Kc = Ac[1,2]/Ac[2,1]*(Ac[2,0]+Ac[2,1]+Ac[2,2])/B[1,1]
        
        self.Kcp = Kcp
        self.Kci = Kci
        self.Kcd = Kcd
        
        self.Vhmax = Vhmax
        self.Vcmax = Vcmax
        self.Khiaw = Khiaw
        self.Kciaw = Kciaw
        
        self.err = track(0)
        
        self.iaw = 0
        
        self.To = To
        
        self.u = np.matrix([[0,0]]).transpose()
        self.s = 0
        self.state = self.HPID
        
        self.sat = False
        
    def update(self,T,Tdes,dt):
        
        self.err.update(Tdes-T[2],dt)
        
        if self.state == self.HPID:
            
            self.u[0,0] = self.PID(self.Khp,self.Khi,self.Khd,self.Khiaw,[0,self.Vhmax],self.Kh*(self.To-Tdes))
            self.u[1,0] = 0
            self.s = 0
            #if (Tdes < self.To) | ():
            if (Tdes>To-1)&(Tdes<To+1)&(T[2]>To-1)&(T[2]<To+1):
                self.state = self.NONE
            elif (1.1*(Tdes-To)+To < T[2]):
                self.state = self.CPID
                self.err.clear()
                
        elif self.state == self.CPID:
            self.u[0,0] = 0
            self.u[1,0] = self.PID(self.Kcp,self.Kci,self.Kcd,self.Kciaw,[self.Vcmax,0],self.Kc*(self.To-Tdes))
            self.s = 1
            #if Tdes > self.To:
            if (Tdes>To-1)&(Tdes<To+1)&(T[2]>To-1)&(T[2]<To+1):
                self.state = self.NONE
            elif 1.1*(Tdes-To)+To > T[2]:
                self.state = self.HPID
                self.err.clear()
                
        elif self.state == self.NONE:
            self.u[0,0] = 0
            self.u[1,0] = 0
            if (Tdes<To-1)|(Tdes>To+1)|(T[2]<To-1)|(T[2]>To+1):
                if Tdes < T[2]:
                    self.state = self.CPID
                else:
                    self.state = self.HPID
                self.err.clear()
        
    def PID(self,Kp,Ki,Kd,Kiaw,Vrange,K):
        
        #K = 0  #Uncomment to use standard control
        
        Vp = Kp*self.err.x
        Vi = Ki*(self.err.i+self.iaw)
        Vd = Kd*self.err.d
        
        Vraw = Vp+Vi+Vd+K
        
        if Vraw >= Vrange[1]:
            V = Vrange[1]
        elif Vraw <= Vrange[0]:
            V = Vrange[0]
        else:
            V = Vraw
        
        self.iaw = (V-Vraw)*Kiaw
        
        return V
        
class kalman:
    
    def __init__(self,R,chmodel,comodel):
        self.comodel = comodel
        self.chmodel = chmodel
        self.R = R
        self.T = self.chmodel.T
        self.P = self.chmodel.P
        
    def update(self,Tmeas,Tdes,dt):
        
        self.comodel.update(self.T,Tdes,dt)
        self.chmodel.update(self.comodel.u,self.comodel.s,dt)
        
        Tprior = self.chmodel.T
        Pprior = self.chmodel.P
        
        y = Tmeas-Tprior

        S = Pprior+self.R
        K = npa.dot(Pprior,npa.inv(S))
        
        self.T = Tprior+npa.dot(K,y)
        self.P = npa.dot((npa.eye(3)-K),Pprior)

        #self.T = Tmeas  # Uncomment to get rid of kalman filtering
        
        self.comodel.T = self.T
        self.chmodel.T = self.T




def Tdes(t):
    if t < 5:
        return 27
    elif (t >10) &(t<40):
        return 200
    else:
        return 200
    

    
    
    
# What to actually use
Ah = np.matrix([[-10,0,10],
      [0,-1,1],
      [7,2,-9.3]])

Ahact = np.matrix([[-10,0,10],
      [0,-1,1],
      [7,2,-9.35]])
  
Ac = np.matrix([[-1,0,1],
      [0,-10,10],
      [2,7,-9.3]])

Acact = np.matrix([[-1,0,1],
      [0,-10,10],
      [2,7,-9.35]])
  
B = np.matrix([[1,0],
     [0,-1],
     [0,0]])


To = 27

Ti = np.matrix([[27,27,27]]).transpose()


Khp = 20;
Khi = 0.01;
Khd = 0.5;

Khiaw = 0.05;
Vhmax = 120;

Kcp = 20;
Kci = 0.01;
Kcd = 0.5;
Kciaw = 5;
Vcmax = 12;

Pi = np.matrix([[1,0,0],[0,1,0],[0,0,1]])
Qh = np.matrix([[0,0,0],[0,0,0],[0,0,0]])
Qc = np.matrix([[0,0,0],[0,0,0],[0,0,0]])

R = np.matrix([[1,0,0],[0,1,0],[0,0,1]])

chamber = model(Ahact,Acact,B,To,Ti,Pi,Qh,Qc)
cont = control(Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B)
kal = kalman(R,model(Ah,Ac,B,To,Ti,Pi,Qh,Qc),control(Khp,Khi,Khd,Kcp,Kci,Kcd,Vhmax,Vcmax,Khiaw,Kciaw,To,Ah,Ac,B))


tmax = 100
dt = 0.05

t = 0


T = Ti

curr_time = time.time()
next_time = curr_time + dt
prev_time = curr_time


dt2 = 0.005
next_time2 = curr_time + dt2
end_time = curr_time + tmax

string = ''


while curr_time < end_time:
    
    curr_time = time.time()
    
    
    # Part of simulation
    Tobs = chamber.T
    
    
    if curr_time > next_time:
        next_time += dt
        kal.update(Tobs,Tdes(t),dt)
        cont.update(T,Tdes(t),dt)
        T = kal.T
        
        string += str(t)+','+str(Tobs[0])+','+str(Tobs[1])+','+str(Tobs[2])+';'
    
    if curr_time > next_time2:
        next_time2 += dt2
        chamber.update(cont.u,cont.s,dt2)
        
        t += dt2


data = string.split(';')
                
                
            
t = numpy.zeros(len(data)-1)
T1 = numpy.zeros(len(data)-1)
T2 = numpy.zeros(len(data)-1)
T3 = numpy.zeros(len(data)-1)
            
for i in range(len(data)-1):

    subdata = data[i].split(',')
    t[i] = float(subdata[0])
    T1[i] = float(subdata[1])
    T2[i] = float(subdata[2])
    T3[i] = float(subdata[3])
    
plt.figure(1)

print(t)
print(T1)
print(T2)
print(T3)

plt.plot(t,T1,'b-',linewidth=1,label='Tdesired')
plt.plot(t,T2,'g-',linewidth=1,label='Tchamber')
plt.plot(t,T3,'r-',linewidth=1,label='Tmeasured')

#plt.plot(tarray,Vh,'g-',linewidth=1,label='u')
plt.xlim([0,tmax])

plt.xlabel('Time, [s]')
plt.ylabel('Temp [C]')
plt.legend(loc='best')
plt.savefig('chamber.png')

os.startfile('chamber.png')

time.sleep(100)