'''
A script that hosts a static server without the use of a wifi network. This 
script uses an access point to host a new lan so that up to 5 devices may connect.
the default IP address is http://192.168.4.1 type this in to a browser while connected
to the AP to view the webpage.

Network Name: Environmental Chamber
Network Password: Environmental Chamber
IP Address: http://192.168.4.1
'''
import network
import esp

esp.osdebug(None)

import gc
gc.collect()

ssid = 'Environmental Chamber'
password = 'Poly'

ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid=ssid, password=password)

while ap.active() == False:
  pass

print('Connection successful')
print(ap.ifconfig())
