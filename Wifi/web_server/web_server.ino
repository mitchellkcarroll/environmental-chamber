#include <Arduino.h>
#ifdef ESP32
  #include <WiFi.h>
  #include <AsyncTCP.h>
#else
  #include <ESP8266WiFi.h>
  #include <ESPAsyncTCP.h>
#endif
#include <ESPAsyncWebServer.h>

#define RXD2 16
#define TXD2 17

AsyncWebServer server(80);
AsyncEventSource events("/events");

const char* ssid = "Environmental Chamber";
const char* password = "cal poly";

const char* PARAM_INPUT_1 = "Setpoint";

int int_TEMP;
unsigned long lastTime = 0;  
unsigned long timerDelay = 1000;

void getstate(){
  int_TEMP +=1;
}

String processor(const String& var){
  getstate();
  if(var == "int_temp"){
    return String(int_TEMP);
}
}

// HTML web page
const char index_html[] PROGMEM = R"rawliteral(
<!DOCTYPE HTML><html><head>
  <html><head> <title>Environmental Chamber</title> 
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" href="data:,"> <style>html{font-family: Helvetica; display:inline-block; margin: 0px auto; text-align: center;}
  h1{color: #0F3376; padding: 2vh;}p{font-size: 1.5rem;}.button{display: inline-block; background-color: #e7bd3b; border: none; 
  border-radius: 4px; color: white; padding: 16px 40px; text-decoration: none; font-size: 30px; margin: 2px; cursor: pointer;}
  .button2{background-color: #4286f4;}</style></head><body> <h1>Environmental Chamber</h1>
  
  <form action="/get">
    Input Setpoint Temperature: <input type="text" name="Setpoint">
    <input type="submit" value="Submit">
  </form><br>

  <p>Setpoint Temperature: </p>
  <p> <strong>  set_temp  </strong></p>
  <p>Internal Temperaure: </p>
  <p> <strong>  int_temp  </strong></p>
  <p>Ambient Temperaure: </p>
  <p><strong> amb_temp  </strong></p>
  <p>Heating Element Temperaure: </p>
  <p><strong>  he_temp  </strong></p>
  <p>Peltier Cold Side Temperaure: </p>
  <p><strong>  ce_temp  </strong></p>
  
  
<script>
if (!!window.EventSource) {
 var source = new EventSource('/events');
 
 source.addEventListener('open', function(e) {
  console.log("Events Connected");
 }, false);
 source.addEventListener('error', function(e) {
  if (e.target.readyState != EventSource.OPEN) {
    console.log("Events Disconnected");
  }
 }, false);
 
 source.addEventListener('message', function(e) {
  console.log("message", e.data);
 }, false);
 
 source.addEventListener('int_TEMP', function(e) {
  console.log("int_TEMP", e.data);
  document.getElementById("temp").innerHTML = e.data;
 }, false);
</script>
</body></html>)rawliteral";

void notFound(AsyncWebServerRequest *request) {
  request->send(404, "text/plain", "Not found");

}
void setup() {
  Serial.begin(115200);
  Serial2.begin(115200,SERIAL_8N1,RXD2,TXD2);

  // Connect to Wi-Fi network with SSID and password
  Serial.print("Setting AP (Access Point)…");
  // Remove the password parameter, if you want the AP (Access Point) to be open
  WiFi.softAP(ssid, password);

  IPAddress IP = WiFi.softAPIP();
  Serial.print("AP IP address: ");
  Serial.println(IP);
  
  // Send web page with input fields to client
  server.on("/", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/html", index_html);
  });

  // Send a GET request to <ESP_IP>/get?input1=<inputMessage>
  server.on("/get", HTTP_GET, [] (AsyncWebServerRequest *request) {
    String inputMessage;
    String inputParam;
    // GET input1 value on <ESP_IP>/get?input1=<inputMessage>
    if (request->hasParam(PARAM_INPUT_1)) {
      inputMessage = request->getParam(PARAM_INPUT_1)->value();
      inputParam = PARAM_INPUT_1;
    }

    else {
      inputMessage = "No message sent";
      inputParam = "none";
    }
    Serial.println(inputMessage);
    Serial2.print(inputMessage);
    
    request->send(200, "text/html", "You have input a setpoint temperature of: " + inputMessage + "&#730;C"
                                     "<br><a href=\"/\">Return to Status Page</a>");
  });
  server.onNotFound(notFound);
    events.onConnect([](AsyncEventSourceClient *client){
    if(client->lastId()){
      Serial.printf("Client reconnected! Last message ID that it got is: %u\n", client->lastId());
    }
    // send event with message "hello!", id current millis
    // and set reconnect delay to 1 second
    client->send("hello!", NULL, millis(), 10000);
  });
  server.addHandler(&events);
  server.begin();
}


void loop(){
 if ((millis() - lastTime) > timerDelay) {
  getstate();
  events.send("ping",NULL,millis());
  events.send(String(int_TEMP).c_str(),"temperature",millis());
  lastTime = millis();
 }
}
