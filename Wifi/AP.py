# -*- coding: utf-8 -*-
"""
Created on Wed May 19 19:57:12 2021

@author: camer
"""

# Complete project details at https://RandomNerdTutorials.com
import ure
try:
  import usocket as socket
except:
  import socket

import network

import esp
esp.osdebug(None)

import gc
gc.collect()

ssid = 'TECH'
password = 'sp'

ap = network.WLAN(network.AP_IF)
ap.active(True)
ap.config(essid=ssid, password=password)

while ap.active() == False:
  pass

print('Connection successful')
print(ap.ifconfig())

def web_page(numb):
  html = """<html><head><meta name="viewport" content="width=device-width, initial-scale=1"></head>
  <body><h1>Hello, World!""" +str(numb) +"""</h1></body>
  <tr>
      <td>Password:</td>
      <td><input name="password" type="password" /></td>
  </tr>
  </table>
      <p style="text-align: center;">
      <input type="submit" value="Submit" /></form>
  </html>""" #% dict()
  
  return html

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.bind(('', 80))
s.listen(5)

numb = ''

while True:
  conn, addr = s.accept()
  print('Got a connection from %s' % str(addr))
  request = conn.recv(1024)
  
  
  match = ure.search("password=(.*)", request)

  if match != None:
      try:
        numb = match.group(1).decode("utf-8").replace("%3F", "?").replace("%21", "!")
      except Exception:
        numb = match.group(1).replace("%3F", "?").replace("%21", "!")
      #print(match)
      
  
  print('Content = %s' % str(request))
  response = web_page(numb)
  conn.send(response)
  conn.close()