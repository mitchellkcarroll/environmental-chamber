# -*- coding: utf-8 -*-
"""
Created on Thu Apr 15 09:45:42 2021

@author: camer
"""

from machine import Pin,PWM,UART
from array import array
import os
import utime
from Temp_driver import temp
from servo import switch



def check_numb(numb):
    '''
    @brief  A function that makes sure the inputted value is a number
    @param  numb    The number to make sure is a number
    '''
    num = numb.encode('ascii')  
    l = len(num)
    dec_count = 0
    is_numb = True
    
    for n in range(l):
        # Check for valid frequency
        if not (num[n] >= ord ('0')) & (num[n] <= ord('9')): 
            #print('Possible problem')
            if num[n] == ord('.'):
                dec_count += 1
                if dec_count > 1:
                    is_numb = False
                    #print('Decimal Problem')
                    return False
            elif n == 0:
                if num[n] != ord('-'):
                    #print('Negative not at start')
                    is_numb = False
                    return False
            elif ((num[n] != 13)&(num[n] != 10)):
                #print(l)
                #print(n)
                #print(num[n])
                #print('Not a number')
                is_numb = False
                return False
    return is_numb




class data_collection:
    
    INIT = 0
    COLLECT = 1
    SUBMIT = 2
    WAIT = 3
    
    def __init__(self,sample_rate, T1,T2,T3,T4):
        
        self.sample_rate = sample_rate
        
        self.start_time = utime.time()
        self.curr_time = utime.time()
        self.next_time = self.curr_time+self.sample_rate
        
        self.state = self.INIT
        
        self.t_array = array('i',[])
        self.T1_array = array('i',[])
        self.T2_array = array('i',[])
        self.T3_array = array('i',[])
        self.T4_array = array('i',[])
        
        self.T1 = T1
        self.T2 = T2
        self.T3 = T3
        self.T4 = T4
        
        self.send_array = ''
        self.count = 1
        
        
    def run(self):
        self.curr_time = utime.time()
        
        if self.curr_time >= self.next_time:
            self.next_time += self.sample_rate
            
            if self.state == self.INIT:
                self.state = self.COLLECT
            elif self.state == self.COLLECT:
                
                self.t_array.append(self.curr_time-self.start_time)
                self.T1_array.append(round(temp(self.T1)))
                self.T2_array.append(round(temp(self.T2)))
                self.T3_array.append(round(temp(self.T3)))
                self.T4_array.append(round(temp(self.T4)))
                
            elif self.state == self.SUBMIT:
                
                self.send_array = 'Time,Ti,Tc,Th,Tout\n'
                
                filename = 'run'+str(self.count)+'.csv'
                self.count += 1
                
                csvfile = open(filename, 'w')
                
                
                for i in range(len(self.T4_array)):
                    
                    self.send_array +=   str(self.t_array[i])+','
                    self.send_array +=   str(self.T1_array[i])+','
                    self.send_array +=   str(self.T2_array[i])+','
                    self.send_array +=   str(self.T3_array[i])+','
                    self.send_array +=   str(self.T4_array[i])+'\n'
                    
                    
                    # writing to csv file 
                        
                    csvfile.write(self.send_array)
                    
                    self.send_array=''
                
                csvfile.close()
                self.t_array = array('i',[])
                self.T1_array = array('i',[])
                self.T2_array = array('i',[])
                self.T3_array = array('i',[])
                self.T4_array = array('i',[])
                    
                self.transition_to(self.WAIT)
                    
    def transition_to(self,state):
        self.state = state
        if state == self.COLLECT:
            self.start_time = utime.time()
            

                
                
class UI:
    
    INIT = 0
    WAIT = 3
    COLLECT = 1
    SUBMIT = 2
    
    def __init__(self,sample_rate,response,data_collection, heater, cooler,servo,coolant):
        
        self.UART = UART(2,115200)
        self.UART.init(115200, bits=8, parity=None, stop=1)
        
        self.bt = UART(1,115200)
        self.bt.init(115200, bits=8, parity=None, stop=1,tx=19,rx=21)
        
        self.response = response
        
        self.heater = heater
        self.cooler = cooler
        self.switch = servo
        self.coolant = coolant
        
        self.coolant.value(0)
        
        self.data_collection = data_collection
        
        self.sample_rate = sample_rate
        
        self.curr_time = utime.time()
        self.next_time = self.curr_time+self.response
        
        self.state = self.INIT
        
        
        
        
    def run(self):
        
        self.curr_time = utime.time()
        self.data_collection.run()

        if self.curr_time >= self.next_time:
            self.next_time += self.response
            
            self.UART.write((';'+str(temp(self.data_collection.T3))).encode('ascii'))
            
            if self.state == self.INIT:
                self.state = self.WAIT
            
            if self.state == self.WAIT:
                #print('wait')
                if self.UART.any() > 0:
                    
                    numb = self.UART.readline().decode('ascii')
                    #print(numb)
                    if check_numb(numb):
                        self.PWM = int(numb)
                    else:
                        self.PWM = 0
                    
                    #print(self.PWM)
                    
                    self.set_temp()
                        
                    self.state = self.COLLECT
                    self.data_collection.transition_to(self.data_collection.COLLECT)
            
            elif self.state == self.COLLECT:
                
                
                if self.UART.any() > 0:
                    
                    numb = self.UART.readline().decode('ascii')
                    #print('numb')
                    if check_numb(numb):
                        self.PWM = int(numb)
                    else:
                        self.PWM = 0
                    #print(self.PWM)
                    self.state = self.SUBMIT
                   
                    self.data_collection.state = self.data_collection.SUBMIT
                    
                    
                    
            elif self.state == self.SUBMIT:
                if self.data_collection.state != self.data_collection.SUBMIT:
                    
                
                    self.set_temp()
                
                    self.state = self.COLLECT
                    self.data_collection.state = self.data_collection.COLLECT
                
    def set_temp(self):
        if self.PWM >= 0:
            PWM(self.heater, freq = 50, duty = self.PWM)
            PWM(self.cooler, freq = 50, duty = 0)
            self.switch.heat()
            self.coolant.value(0)
            
            l = Pin(2,Pin.OUT)
            l.value(1)
            #print('heating')
                
        else:
            PWM(self.heater, freq = 50, duty = 0)
            PWM(self.cooler, freq = 50, duty = -self.PWM)
            self.switch.cool()
            self.coolant.value(1)
                
            l = Pin(2,Pin.OUT)
            l.value(0)
            #print('cooling')
                
if __name__ == "__main__":
    
    
    
    sample_rate = 1
    response = 0.1
    # define T1,T2,T3,pinh,pinc
    T1 = Pin(33)
    T2 = Pin(32)
    T3 = Pin(35)
    T4 = Pin(34)
    
    #print('print works')
    
    
    pinh = Pin(25)
    pinc = Pin(18)
    
    s = switch(Pin(4),-960,-940)
    
    fan = Pin(26,Pin.OUT)
    fan.value(1)
    
    coolant = Pin(22,Pin.OUT)
    
    dc = data_collection(sample_rate,T1,T2,T3,T4)
    ui = UI(sample_rate,response,dc,pinh,pinc,s,coolant)
    
    while True:
        ui.run()