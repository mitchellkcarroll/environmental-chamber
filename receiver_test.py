from machine import UART
myuart = UART(2)
while True:
    if myuart.any() != 0:
        val = myuart.readchar()
        myuart.write('You sent an ASCII '+ str(val) +' to the ESP32')
        print('got it')