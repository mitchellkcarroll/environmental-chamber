# -*- coding: utf-8 -*-
"""
Created on Tue May 25 11:20:29 2021

@author: camer
"""

from machine import Pin,PWM

class switch:
    def __init__(self, pin, cold, hot):
        
        self.c = cold
        self.h = hot
        
        self.pin = pin
        
        self.PWM = self.c
        
    def heat(self):
        PWM(self.pin,freq =50,duty = self.h)
    def cool(self):
        PWM(self.pin,freq =50,duty = self.c)
        
if __name__ == "__main__":
    
    s = switch(Pin(4),60,80)