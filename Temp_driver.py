'''
@file Temp_drivver
@brief this file contains a driver for thermistors
@author Mitchell Carroll
'''

from machine import ADC
import math as m


def temp(pin):
    '''
    A function that retuns the temperature in celcius when called
    '''
    R1 = 100000

    therm = ADC(pin)
    therm.atten(ADC.ATTN_11DB)
        
    V =therm.read()
    
    if V != 0:
        R2 = R1*(4095/V-1)
    else:
        R2 = 0

    #temp = 1/((1/298.15) + (1/3950)*(m.log(R2/R1))) - 273.15
    
    #while temp == 0:
        
    #    V =therm.read()

    #    R2 = R1*(4095/V-1)
    
    #    temp = 1/((1/298.15) + (1/3950)*(m.log(R2/R1))) - 273.15
        
    return R2
